var taxi = new Taxi();
var KeyPress = new KeyPress();
var closestTrafficLight = taxi.closestTrafficLight();

KeyPress.right(function() {
  var closestTrafficLight = taxi.closestTrafficLight();
    if (closestTrafficLight.currentColor() !== "red") {
      taxi.forward();
      displayMessage("GO!.");
    }
  })

   KeyPress.left(function() {
     var closestTrafficLight = taxi.closestTrafficLight();
    if (closestTrafficLight.currentColor() !== "red") {
      taxi.reverse();
    }
    })

     KeyPress.up(function() {
       var closestTrafficLight = taxi.closestTrafficLight();
      if (closestTrafficLight.currentColor() === "green") {
        closestTrafficLight.makeOrange();
        displayMessage("Slow down!.");
      } else if (closestTrafficLight.currentColor() === "orange") {
        closestTrafficLight.makeRed();
        displayMessage("STOP!.");
      }
         else {
             closestTrafficLight.makeGreen();
             displayMessage("GO!.");
           }

      })

  KeyPress.down(function(){
    var closestTrafficLight = taxi.closestTrafficLight();
    if (closestTrafficLight.currentColor() === "red") {
      displayMessage("GO!.");
      closestTrafficLight.makeGreen();
    } else if (closestTrafficLight.currentColor() === "green") {
      displayMessage("Slow down!.");
      closestTrafficLight.makeOrange();
    } else {
      closestTrafficLight.makeRed();
      displayMessage("STOP!.");
    }
  })
